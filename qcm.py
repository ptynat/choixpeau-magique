
from browser import document, console, html

QUESTIONS = [
    {"question1":
        {
            "rep1": (1, 1, 1, 1),
            "rep2": (1, 2, 3, 4),
            "rep3": (0, 1, 1, 1),
            "rep4": (4, 3, 1, 2) }},
    {"question2":
        {
            "rep5": (1, 1, 1, 1),
            "rep6": (1, 2, 3, 4),
            "rep7": (0, 1, 1, 1),
            "rep8": (4, 3, 1, 2) }},
    
    {"question3":
        {
            "rep9": (1, 1, 1, 1),
            "rep10": (1, 2, 3, 4),
            "rep11": (0, 1, 1, 1),
            "rep12": (4, 3, 1, 2) }}
    ]

QUESTION_LIST = ["question1", "question2", "question3"]
TITLE = "titleq"
CHOICES = "choicesq"
compteur = 0
question_id = 0

def divchoice( value, content):
     return html.B(f"""
            <div id={content}>
                <button id="rep{value + 1}">{content}</button>
            </div>
            """)

#: dict[str, tuple(int, int, int, int)]

def createChoices(choices):
    div = ""
    global question_id
    for k, _ in choices.items():
        div += divchoice( question_id, k)
        question_id += 1
    return div

#: dict[str, dict[str, tuple(int, int, int, int)]]

def CreateQuestion(question):

    div = ""
    index_q = 0
    for k, v in question.items():
        document[TITLE].textContent = k
        choices = createChoices(v)
        document[CHOICES] <= choices
        index_q += 1
    global question_id
    for i in range(question_id): #marche pas 
        document["rep"+str(question_id)].bind("click", next_q)
    return div

def hide(question):
    for v,_ in question.items():
            display = document[v].style.display
            document[v].style.display = "inline" if display == "none" else "none"

def start(ev):

    return CreateQuestion(QUESTIONS[0])

def next_q(ev):
    global compteur
    compteur += 1
    return    hide(QUESTIONS[compteur-1][QUESTION_LIST[compteur-1]]), CreateQuestion(QUESTIONS[compteur])

def hide_start_button(ev):
    display = document["start"].style.display
    document["start"].style.display = "inline" if display == "none" else "none"


document["start"].bind("click", start)
document["start"].bind("click", hide_start_button)
document["nextq"].bind("click", next_q)
