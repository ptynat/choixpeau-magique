from browser import document, bind, console, html
from main import choose_house_counter

QUESTION = {
    "question1":
        {
            "rep1": (1, 1, 1, 1),
            "rep2": (1, 2, 3, 4),
            "rep3": (0, 1, 1, 1),
            "rep4": (4, 3, 1, 2)
        },
        "question2":
        {
            "rep2_1": (1, 1, 1, 1),
            "rep2_2": (1, 2, 3, 4),
            "rep2_3": (0, 1, 1, 1),
            "rep2_4": (4, 3, 1, 2)
        }
}

# IDs
TITLE = "title"
FORM = "form"
NEXT = "next"
FINAL = "final"
QDIV = "qq"
QI_ = "q"
QL_ = "l"
ANS = "answer"


def to_list(d):
    return [(k, v) for k, v in d.items()]


# Properly change questions
CONV_Q = to_list(QUESTION)  # converted dict into list for better usage
COUNTER = 0  # which question are we at
LENGTH = len(CONV_Q)


def hide(id):
    document[id].style.display = "none"


def show(id):
    document[id].style.display = "inline"


@bind(f"#{NEXT}", "click")
def next(e):
    global COUNTER
    cq = CONV_Q[COUNTER]
    createQuestion(cq[0], cq[1])
    COUNTER += 1
    for nq in range(4):
        console.log(document[f"q{nq+1}"].checked)
    check_final()


@bind(f"#{FINAL}", "click")
def final(e):
    for nq in range(4):
        console.log(document[f"q{nq+1}"].checked)


def createQuestion(title, q):
    document[TITLE].textContent = title
    i = 0
    for ans, val in q.items():
        document[f"{QI_}{i+1}{QL_}"].textContent = ans
        i += 1


def check_final():
    if COUNTER == LENGTH:
        hide(NEXT)
        show(FINAL)


next(None)
